from fastapi import APIRouter, Depends, status
from .. import schemas, database, oauth2
from typing import List
from sqlalchemy.orm import Session
from ..repository import blog

get_db = database.get_db
router = APIRouter(
    tags=["Blogs"]
)


@router.get('/blog', response_model=List[schemas.ShowBlog])
def get_all_blogs(db: Session = Depends(get_db), current_user: schemas.User = Depends(oauth2.get_current_user)):
    return blog.get_all(db)


@router.post('/blog', status_code=status.HTTP_201_CREATED)
def create(request: schemas.Blog, db: Session = Depends(
    get_db), current_user: schemas.User = Depends(
    oauth2.get_current_user)):  # Whenever user wants to send something to API we write request: schemas.Blog
    return blog.create(db, request)


@router.delete(
    '/blog/{id}')  # adding this as parameter status_code=status.HTTP_204_NO_CONTENT will throw an error because of some required conditions need to fullfilled for 204 error code
def destroy(id: int, db: Session = Depends(get_db), current_user: schemas.User = Depends(oauth2.get_current_user)):
    return blog.destroy(db, id)


@router.put('/blog/{id}')
def update(id: int, request: schemas.Blog, db: Session = Depends(get_db),
           current_user: schemas.User = Depends(oauth2.get_current_user)):
    return blog.update(db, request, id)


@router.get('/blog/{id}', status_code=200,
            response_model=schemas.ShowBlog)  # Response Model is actually a response schema used to show particular things only. For eg: title and body
def show(id: int, db: Session = Depends(get_db), current_user: schemas.User = Depends(oauth2.get_current_user)):
    return blog.show(db, id)
