from fastapi import APIRouter, Depends
from .. import database, schemas, oauth2
from sqlalchemy.orm import Session
from typing import List
from ..repository import user

router = APIRouter(
    tags=["Users"]
)
get_db = database.get_db


@router.post('/user', response_model=schemas.ShowUser)
def create_user(request: schemas.User, db: Session = Depends(get_db), current_user: schemas.User = Depends(oauth2.get_current_user)):
    return user.create_user(db, request)


@router.get('/user/{id}', response_model=schemas.ShowUser)
def get_user(id: int, db: Session = Depends(get_db), current_user: schemas.User = Depends(oauth2.get_current_user)):
    return user.get_user(db, id)


@router.get('/user', response_model=List[schemas.ShowUser])
def get_all_users(db: Session = Depends(get_db), current_user: schemas.User = Depends(oauth2.get_current_user)):
    return user.get_all_user(db)
